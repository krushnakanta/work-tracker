import React,{useState} from 'react';
import  './Signin.css';
import {useHistory} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Signup from './Signup';

const Signin =( ) =>{
  const [username,setUserName]=useState('');
  const [password,setPassword]=useState('');
  const [user_level,SetUser_level]=useState(false);
  let history =useHistory();

  
  const onSubmitHandler = () => {
        
    let empInfo={username,password};
    console.log(empInfo);
   fetch('http://127.0.0.1:8000/login',{
        method:'POST',

        headers:{
          'Content-type':'application/json'
        },
        body: JSON.stringify(empInfo)
    }).then(r=>r.json()).then(res=>{
     
        console.log(res)
      localStorage.setItem("Data",JSON.stringify(res))
        // console.log(Data)
        // alert(res)
        if(res==400){
          // alert('You Successfully done Signup')
          alert("Incorrect username or password")
          window.location.reload(false)
          // history.push("/dashboard")
        }
        else{
          history.push("/dashboard")
        }
    })
}



  //push to signup component  
    const Signup =()=>{
      console.log('hi')
      history.push('./signup')
    }


    return(
        <>

          <div class="container" >
              <div class="row">
                <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                  <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center text-darkyellow">Sign In</h5>
                          <div class="form-signin">
                              <div class="form-label-group">
                                <input
                                  type="email"
                                  id="inputEmail"
                                  class="form-control"
                                  placeholder="Email address"
                                  onChange={e => setUserName(e.target.value)}
                                  /> 
                                <label for="inputEmail">Username</label>

                              </div>
                              <div class="form-label-group">
                                <input
                                  type="password"
                                  id="inputPassword"
                                  class="form-control"
                                  placeholder="Password"
                                  onChange={e => setPassword(e.target.value)}
                                  />
                                <label for="inputPassword">Password</label>
                              </div>
                              {/* <div>
                                    <input type="checkbox" 
                                    class="col-sm-2"
                                    onChange={e=> SetUser_level(!user_level)} 
                                    />
                                    <label className="text-dark">Login as Admin?</label>
                              </div> */}
                              <button
                                class="btn btn-lg btn-primary btn-block text-uppercase"
                                type="submit"
                                onClick={onSubmitHandler}
                              >
                              Login </button>
                           
                              <label>Do not have an account?</label>
                              <label><a className="text-info" onClick={Signup}>SIGN UP</a></label>
                          </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        
        </>

    )
}

export default Signin;
