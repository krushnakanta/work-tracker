import React, { Component,useState,useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header';
import SidebarNav from './SidebarNav';
import { Link } from 'react-router-dom';
import { Table, Button, Dropdown,} from 'react-bootstrap';
import { Autocomplete } from '@material-ui/lab';
import Typography from '@material-ui/core/Typography';
import Pagination from '@material-ui/lab/Pagination';
import { TextField,TablePagination } from '@material-ui/core';
import axios from 'axios';
import AddProject from './AddProject.jsx';
// import Pagination from './Pagination';

 const Dashboard= () =>{
    let [menuState, setMenuState] = useState(false);
    const[dashboard_details,setDashboard_Details]=useState('');
    const[projectInfo,setProjectInfo]=useState([]);
    const [title,setTitle]=useState('');
    const [description,setDescription]=useState('');
    const[isActive,setIsActive]=useState('');
    const[owner,setOwner]=useState('');
    const[startDate,setStartDate]=useState('');
    const[endDate,setEndDate]=useState('');
    const [permit,setPermit]=useState([]);
    const[auto,setAuto]=useState([]);
    const[page,setPage]=useState(1);
    const [modalShow, setModalShow] = React.useState(false);
    const Data = JSON.parse(localStorage.getItem("Data")) ;
    const headers = {
       Authorization: `Bearer ${Data.token}`
      }


 useEffect(() => {
  axios.get(`http://127.0.0.1:8000/project_list?page=${page}&per_page=3`, 
  {
    headers: headers
  })
    .then(function (response) {
      console.log(response.data.results)

      setProjectInfo(response.data.results)

          // let projectList = [];
          //     response.data.results.forEach((elem) => {
          //     projectList.push({ title: elem.name });
          //     });
          // setAuto(projectList);
      
    })
    .catch(function (error) {
      console.log(error);
    }); 
   
 }, [page])

      
  const handleChange = (event, page) => {
    setPage(page);
    console.log(page)
    
    }



    //   let searching={abc:projectInfo.name}

    //   calling dashboard deatails api
    // useEffect(() => {
    //     fetch(
    //       `http://127.0.0.1:8000/dashboard_details`,
      
    //       {
    //         method: "GET",
    //         headers: headers,
    //       }
    //     )
    //     .then(r=>r.json()).then(res=>{
    //       console.log(res)
    //       setDashboard_Details(res)
    //     })
        
    //       .catch(error => console.log(error));
    //   }, []);

// const handleChangePage=(event,newPage)=>{
//   setPage(newPage);
// }




//    calling all project info api
    // useEffect( () => {
    //     axios.get(`http://127.0.0.1:8000/project_list`, 
    //     {
    //       headers: headers
    //     })
    //       .then(function (response) {
    //         console.log(response.data.results)

    //         setProjectInfo(response.data.results.reverse())

    //             let projectList = [];
    //                 response.data.results.forEach((elem) => {
    //                 projectList.push({ title: elem.name });
    //                 });
    //             setAuto(projectList);
            
    //       })
    //       .catch(function (error) {
    //         console.log(error);
    //       }); 
    // },[] )
    console.log(projectInfo)

// calling user information request api

    // useEffect( () => {
    //     axios.get(`http://127.0.0.1:8000/users/activate`, 
    //     {
    //     headers: headers
    //     })
    //     .then(function (response) {
           
    //         setPermit(response.data)
           
            
    //     })
    //     .catch(function (error) {
    //         console.log(error);
    //     }); 
    // },[] )
   


   //add project
    console.log(projectInfo)
    // const onClickHandler=()=>{
    //     console.log("add proj")
    //     console.log(title,description,isActive,startDate,owner,endDate)
    //     axios.post(`http://127.0.0.1:8000/create_project`, {title,description,isActive,startDate,owner,endDate},
    //     {
    //       headers: headers
    //     })
    //       .then(function (response) {
            
    //         setProjectInfo(response.data.projects)
            
    //       })
    //       .catch(function (error) {
    //         console.log(error);
    //       }); 
    // }

//delete project
    const deleteHandler = (e) =>{
        console.log("DEleted")
        let a=e.target.value;
        console.log(a)
        axios.delete(`http://127.0.0.1:8000/projects/${a}/`, 
        {
          headers: headers
        })
          .then(function (response) {
            
            setProjectInfo(response.data.projects)
            
          })
          .catch(function (error) {
            console.log(error);
          }); 
          window.location.reload(false)
    }
    //give permission 
    const permitHandler = (e) =>{
        console.log("hello from admin permission")
        let id=parseInt(e.target.value);
        let is_active=true;
        console.log(id);
        axios.put(`http://127.0.0.1:8000/users/activate`, [{id,is_active}],
        {
          headers: headers
        })
          .then(function (response) {
            console.log(response)
            // setProjectInfo(response.data.projects)
                    
            
          })
          .catch(function (error) {
            console.log(error);
          }); 
          window.location.reload(false)
    }

  


 return (
            <>
            <Header menuState={menuState} setMenuState={setMenuState}/>
            {/* <SidebarNav menuState={menuState} setMenuState={setMenuState}/> */}
                
            <div className='dashboardUser-container'>
        <div className='d-flex mt-4 justify-content-around'>
          <div className='contents-container'>
            {/* <h5>Search</h5> */}
            <Autocomplete
              id='combo-box-demo'
              options={auto}
              getOptionLabel={(option) => option.title}
              style={{ width: 180 }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Search Project'
                  variant='outlined'
                />
              )}
            />
          </div>
          <div className='contents-container'>
            <Dropdown className='sprints'>
              <Dropdown.Toggle id='dropdown-basic'>Sprints</Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>dfghj</Dropdown.Item>
                <Dropdown.Item>efgb</Dropdown.Item>
                <Dropdown.Item>qasfghj</Dropdown.Item>
                <Dropdown.Item>qwertyui</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          {/* <div className='contents-container'>
            <h5>Search</h5>
            <Autocomplete
              id='combo-box-demo'
              options={[]}
              getOptionLabel={(option) => option.title}
              style={{ width: 180 }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Search Owner'
                  variant='outlined'
                />
              )}
            />
          </div> */}
         


        </div>
        <div className='mt-4'>
          <div className='d-flex justify-content-between add-task'>
            <h4>List of Projects</h4>
            <Button variant="primary" onClick={() => setModalShow(true)}>
                Add Project
            </Button>

                <AddProject
                    show={modalShow}
                    onHide={() => setModalShow(false)}
                />
          </div>


          <Table className='table table-hover'>
            <thead>
              <tr>
                <th>SL.NO</th>
                <th>Title</th>
                <th>Owner</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Is Active</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            {projectInfo.map((d,index) => {
                         return (
                                <tr key={index}>
                                <td>{index+1} </td>
                                <td>{d.name}</td>
                                <td>{d.owner.username}</td>
                                <td>{d.start_date}</td>
                                <td>{d.end_date}</td>
                                <td>{d.is_active?"True":"False"}</td>
                                <td>
                                    <Link to={`/edit/user/${d.id}`}  class="mr-2">
                                        <Button>Edit</Button>
                                    </Link>
                                    
                                      <Button onClick={deleteHandler} value={d.id}>Delete</Button>
                                    
                                    
                                </td>
                                
                                 </tr>
                                    )
                           })} 

            </tbody>
         </Table> 
         {/* <Pagination
          className='p-3 w-100'
          count={Math.ceil(projectInfo.length/2)}
          color='primary'
          rowsPerPage={3}
          showFirstButton
          showLastButton
          page={page}
          onChange={handleChangePage}
         /> */}
         <Typography>Page: {page}</Typography>
        <Pagination 
          count={Math.ceil(projectInfo.length/1)}
        // rowsPerPage={3}
        page={page} onChange={handleChange} />
       </div>
      </div>
     
    </>
        );

    }

export default Dashboard;





