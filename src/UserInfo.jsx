import React, { Component,useState,useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header';
import SidebarNav from './SidebarNav';
import { Link } from 'react-router-dom';
import { Table, Button, Dropdown } from 'react-bootstrap';
import { Autocomplete } from '@material-ui/lab';
import { TextField } from '@material-ui/core';

import axios from 'axios';

 const UserInfo= () =>{
    let [menuState, setMenuState] = useState(false);
    const[dashboard_details,setDashboard_Details]=useState('');
    const[projectInfo,setProjectInfo]=useState([]);
    const [title,setTitle]=useState('');
    const [description,setDescription]=useState('');
    const[isActive,setIsActive]=useState('');
    const[owner,setOwner]=useState('');
    const[startDate,setStartDate]=useState('');
    const[endDate,setEndDate]=useState('');
    const [permit,setPermit]=useState([]);
    const [status,setStatus]=useState('');
    const Data = JSON.parse(localStorage.getItem("Data")) ;
    const headers = {
       Authorization: `Bearer ${Data.token}`
      }

    // useEffect( () => {
    //     axios.get(`http://127.0.0.1:8000/project_list`, 
    //     {
    //       headers: headers
    //     })
    //       .then(function (response) {
    //         // console.log(response.data)
    //         setProjectInfo(response.data)
            
    //       })
    //       .catch(function (error) {
    //         console.log(error);
    //       }); 
    // },[] )
    // console.log(projectInfo)

// calling pending request api
    useEffect( () => {
        axios.get(`http://127.0.0.1:8000/users/activate`, 
        {
        headers: headers
        })
        .then(function (response) {
        //    console.log(response)
            setPermit(response.data.results)
            
            
        })
        .catch(function (error) {
            console.log(error);
        }); 
    },[] )

    useEffect( () => {
      axios.get(`http://127.0.0.1:8000/projects/1/task/1/`, 
      {
      headers: headers
      })
      .then(function (response) {
         console.log(response)
         
          
          
      })
      .catch(function (error) {
          console.log(error);
      }); 
  },[] )
   console.log(permit)

  const a= permit.map((a,index)=>a.activate)
//   if(a==="activate"){
//     console.log("hi")
//   }
// console.log(a=="activate")
//give permission 
    const permitHandler = (e,a,x) =>{
        // console.log("hello from admin permission")
        
        console.log(e,a)
        // let id=parseInt(e.target.value);
        // let is_active=true;
        // console.log(id);
        
        axios.put(`http://127.0.0.1:8000/users/activate`, [{id:a,activate:x}],
        {
          headers: headers
        })
          .then(function (response) {
            console.log(response)
            // setProjectInfo(response.data.projects)
            
          })
          .catch(function (error) {
            console.log(error);
          }); 
          window.location.reload(false)
    }


    
    



        return (
            <>
            
            <Header menuState={menuState} setMenuState={setMenuState}/>
            {/* <SidebarNav menuState={menuState} setMenuState={setMenuState}/> */}
                
            <div className='dashboardUser-container'>
        <div className='d-flex mt-4 justify-content-around'>
          <div className='contents-container'>
            {/* <h5>Search</h5> */}
            <Autocomplete
              id='combo-box-demo'
              options={[]}
              getOptionLabel={(option) => option.title}
              style={{ width: 180 }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Search Name'
                  variant='outlined'
                />
              )}
            />
          </div>
          {/* <div className='contents-container'>
            <h5>Search</h5>
            <Autocomplete
              id='combo-box-demo'
              options={[]}
              getOptionLabel={(option) => option.title}
              style={{ width: 180 }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label='Search Status'
                  variant='outlined'
                />
              )}
            />
          </div> */}
         


        </div>
        <div className='mt-4'>
          <div className='d-flex justify-content-between add-task'>
            <h4>List Of Pending Request</h4>
            
          </div>


          <Table className='table table-hover'>
            <thead>
              <tr>
                <th>User ID</th>
                <th>UserName</th>
                <th>First Name</th>
               
                <th>Email</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            {permit.map((d,index) => {
                         return (
                              <tr key={index}>
                                <td>{d.id} </td>
                                <td>{d.username}</td> 
                                <td>{d.first_name}</td>
                                <td>{d.email}</td>
                                {/* <td>{d.activate}</td> */}
                                <td>
                                {d.activate ==0?
                                  "Pending":null}
                                  {d.activate ==1?
                                  "Active":null}
                                  {d.activate ==2?
                                  "Deactivate":null}
                                  {/* {d.activate ==3?
                                  "Deleted":null} */}
                                 </td>
                               
                               
                               
                                <td>
                               {
                                 d.activate ==0 
                                  ?<Button onClick={e=>permitHandler(d.activate,d.id,1)}>Active</Button>
                                  :null
                                }  
                                {
                                 d.activate ==1 
                                  ?<Button onClick={e=>permitHandler(d.activate,d.id,2)}>Deactive</Button>
                                  :null
                                }
                                {
                                 d.activate ==2 
                                  ?<Button onClick={e=>permitHandler(d.activate,d.id,1)}>Active</Button>
                                  :null
                                }
                                {/* {
                                 d.activate =="Delete" 
                                  ?null
                                  :null
                                } */}

                                </td>
                                <td><Button onClick={e=>permitHandler(d.activate,d.id,3)}>Delete</Button></td>
                              </tr>
                                    )
                           })} 

            </tbody>
         </Table> 
        </div>
      </div>
    </>
        );
        
    }

export default UserInfo;






