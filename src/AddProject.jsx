import React,{useState} from 'react';
import { Modal, Button, Form, Col } from 'react-bootstrap';
import { Autocomplete } from '@material-ui/lab';
import { TextField } from '@material-ui/core';
import axios from 'axios';

function AddProject(props) {
    const [title,setTitle]=useState('');
    const [description,setDescription]=useState('');
    const[isActive,setIsActive]=useState('');
    const[startDate,setStartDate]=useState('');
    const[endDate,setEndDate]=useState('');

    const Data = JSON.parse(localStorage.getItem("Data")) ;
    const headers = {
       Authorization: `Bearer ${Data.token}`
      }
    const onClickHandler=()=>{
        console.log("add proj")
        console.log(title,description,isActive,startDate,endDate)
        axios.post(`http://127.0.0.1:8000/create_project`, [{name:title,description:description,is_active:isActive,start_date:startDate,end_date:endDate}],
        {
          headers: headers
        })
          .then(function (response) {
            console.log(response)
            // setProjectInfo(response.data.projects)
            
          })
          .catch(function (error) {
            console.log(error);
          }); 
          window.location.reload(false)
    }



    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
           Create Project
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div>
                                                                 <div className="form-group">
                                                                    {/* <label htmlFor="bookname" >Book Name</label> */}
                                                                        <input 
                                                                        type="text"
                                                                        name="title"
                                                                        autoComplete="off"
                                                                        placeholder="Project Title"
                                                                        // value={bookName}
                                                                        className="form-control"
                                                                        onChange={(e)=>setTitle(e.target.value)}

                                                                        />
                                                                    </div>
                                                                    
                                                                    <div className="form-group">
                                                                    {/* <label htmlFor="grade">Grade</label> */}
                                                                        <input noValidate id="grade"
                                                                        type="text"
                                                                        name="description"
                                                                        placeholder="Description"
                                                                        // value={grade}
                                                                        className="form-control"
                                                                        onChange={(e)=>setDescription(e.target.value)}

                                                                        />
                                                                    </div>

                                                                    {/* <div className="form-group">
                                                                    <label htmlFor="subject">Subject</label>
                                                                        <input noValidate id="subject"
                                                                        type="text"
                                                                        name="owner"
                                                                        placeholder="Owner"
                                                                        // value={subject}
                                                                        className="form-control"
                                                                        // onChange={(e)=>setOwner(e.target.value)}

                                                                        />
                                                                    </div> */}

                                                                    <div className="form-group">
                                                                    {/* <label htmlFor="createdby">Created-By</label> */}
                                                                        <input noValidate id="createdby"
                                                                        type="date"
                                                                        name="startdate"
                                                                        placeholder="Start Date"
                                                                        // value={createdBy}
                                                                        className="form-control"
                                                                        onChange={(e)=>setStartDate(e.target.value)}

                                                                        />
                                                                    </div>

                                                                    <div className="form-group">
                                                                    {/* <label htmlFor="createdby">Created-By</label> */}
                                                                        <input noValidate id="createdby"
                                                                        type="date"
                                                                        name="enddate"
                                                                        placeholder="End Date"
                                                                        // value={createdBy}
                                                                        className="form-control"
                                                                        onChange={(e)=>setEndDate(e.target.value)}

                                                                        />
                                                                    </div>

                                                                    <div className="form-group">
                                                                    {/* <label htmlFor="createdby">Created-By</label> */}
                                                                        <input noValidate id="createdby"
                                                                        type="text"
                                                                        name="Isactive"
                                                                        placeholder="Is Active"
                                                                        // value={createdBy}
                                                                        className="form-control"
                                                                        onChange={(e)=>setIsActive(e.target.value)}

                                                                        />
                                                                    </div>

                                                                    <button className="btn btn-success mt-3 mr-2"
                                                                     onClick={onClickHandler}
                                                                     >
                                                                        Submit 
                                                                    </button>
                                                        </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  export default AddProject;


