
import React, { useState } from 'react';
import './Signin.css';
import {useHistory} from 'react-router-dom';


const Signup =() =>{
    const [username,setUserName]=useState('');
    const [email,setEmail]=useState('');
    const [password,setPassword]=useState('');
    const [first_name,setFirst_name]=useState('');
    const [last_name,setLast_name]=useState('');
    const [user_level,SetUser_level]=useState(false);
    let history = useHistory();
    const onSubmitHandler = () => {
        
        let empInfo={username,email,password,first_name,last_name};
        console.log(empInfo);
       fetch('http://127.0.0.1:8000/register',{
            method:'POST',
  
            headers:{
              'Content-type':'application/json'
            },
            body: JSON.stringify(empInfo)
        }).then(r=>r.json()).then(res=>{
         
            console.log(res)
            if(res.status!==null){
              alert('You Successfully done Signup')
              history.push("/signin")
            }
        })
  }
    

  const Signin =()=>{
    console.log('hi')
    history.push('./signin')
  }
  
  
    
    return(
      <>
        <div class="container">
          <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
              <div class="card card-signin my-5">
                <div class="card-body">
                  <h5 class="card-title text-center">Register</h5>
                  <div class="form-signin">
                  <div class="form-label-group">
                      <input
                       
                        type="text"
                        id="Firstname"
                        class="form-control"
                        placeholder="Username"
                        onChange={e => setFirst_name(e.target.value)}
                        />
                      <label for="Firstname">First Name</label>
                    </div>
                    <div class="form-label-group">
                      <input
                        type="text"
                        id="Lastname"
                        class="form-control"
                        placeholder="Username"
                        onChange={e => setLast_name(e.target.value)}
                        />
                      <label for="Lastname">Last Name</label>
                    </div>
                  <div class="form-label-group">
                      <input
                        type="text"
                        id="inputUsername"
                        class="form-control"
                        placeholder="Username"
                        onChange={e => setUserName(e.target.value)}
                        />
                      <label for="inputUsername">Username</label>
                    </div>
                    <div class="form-label-group">
                      <input
                        type="password"
                        id="inputPassword"
                        class="form-control"
                        placeholder="Password"
                        onChange={e => setPassword(e.target.value)}
                        />
                      <label for="inputPassword">Password</label>
                    </div>
                    <div class="form-label-group">
                      <input
                        type="email"
                        id="inputEmail"
                        class="form-control"
                        placeholder="Email address"
                        onChange={e => setEmail(e.target.value)}
                        />
                      <label for="inputEmail">Email address</label>
                    </div>
                    {/* <div>
                        <input type="checkbox" 
                        class="col-sm-2"
                        onChange={e=> SetUser_level(!user_level)} 
                        />
                        <label className="text-dark">Register as Admin?</label>
                    </div> */}
                    <button
                      class="btn btn-lg btn-primary btn-block text-uppercase"
                      type="submit"
                      onClick={onSubmitHandler}
                    >
                      Register
                    </button>
                      <label>Already have an account?</label>
                      <label><a className="text-info" onClick={Signin}>SIGN IN</a></label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
    
  }
 
export default Signup;